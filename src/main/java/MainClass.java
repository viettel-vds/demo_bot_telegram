import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.util.Scanner;

/* Created by ManhPD on 1/25/2021 */
public class MainClass {
    public static void main(String[] args) {
        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(new TSManhBot());
            SendMessageTelegram sendMessageTelegram = new SendMessageTelegram();
            Scanner input = new Scanner(System.in);  // Create a Scanner object
            String text = "";
            while (true){
                System.out.println("Input message (-1 to exit): ");
                text = input.nextLine();
                sendMessageTelegram.sendToTelegram(text);
                if(text.compareTo("-1") ==0) break;
            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
